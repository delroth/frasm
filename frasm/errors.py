class BadAssemblerError(Exception):
    '''
    Raised when an error is found in the built assembler.

    Be careful, some kind of errors can be detected at runtime only.
    '''
    pass

class AssemblyError(Exception):
    '''
    Raised when an error is found in the input code.
    '''

    def __init__(self, message, location):
        super(AssemblyError, self).__init__(message, location)
