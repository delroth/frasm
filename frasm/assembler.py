import funcparserlib.parser as fpl
from lexy import (
    BaseLexer,
    Newline, Symbol, Keyword, Identifier, Integer, String,
    newline, symbol, keyword, identifier, integer, string
)
from lexy.token import Token as BaseToken, Location

from frasm import ast
from frasm.errors import BadAssemblerError, AssemblyError
from frasm.utils import get_sequence_location, or_all, repr_location


class BaseAssembler:

    def __init__(self, **kwargs):
        # If enabled, opcodes that do not define a static size will issue a
        # warning.
        # Defining a static size allows the assembler not to compute the whole
        # opcode when computing addresses of data and opcodes, thus tracing
        # opcodes which don’t is important.
        self.warning_lazy_opcode_size = kwargs.get(
            'warning_lazy_opcode_size', True)

        # If False, raise an error when symbols are used but not defined.
        self.allow_undefined_symbols = kwargs.get(
            'allow_undefined_symbols', False)

        # If False, mnemonics, register names, keywords and section names will
        # be case insensitive
        self.case_sensitive = kwargs.get('case_sensitive', True)

        # The list of available sections. "None" means: as many as you want.
        # The empty list means: there is no section directive.
        self.section_names = kwargs.get('sections', None)
        if self.section_names and not self.case_sensitive:
            self.section_names = tuple(
                name.lower() for name in self.section_names)

        # The list of available data types. See the "DataType" class.
        self.data_types = {}
        for data_type in kwargs.get('data_types', ()):
            if not self.case_sensitive:
                data_type.name = data_type.name.lower()
            if data_type.name in self.data_types:
                raise BadAssemblerError(
                    'Duplicate data type name: {}'.format(data_type.name))
            self.data_types[data_type.name] = data_type

        # The list of available instructions. See the "BaseOpcode" class.
        self.opcodes = {}
        for opcode in kwargs.get('opcodes', ()):
            if not self.case_sensitive:
                opcode.mnemonic = opcode.mnemonic.lower()
            opcodes_by_mnemonic = self.opcodes.setdefault(opcode.mnemonic, [])
            opcodes_by_mnemonic.append(opcode)

        # Build the set of every needed operand parser looking at each operand
        # of each opcode and make the list of the additional lexer symbols
        # needed by these parsers.
        self.operand_parsers = {}
        self.additional_lexer_symbols = set()
        for opcode_list in self.opcodes.values():
            for opcode in opcode_list:
                for operand in opcode.operands:
                    if not self.case_sensitive:
                        operand.set_case_insensitive()
                    for parser in operand.parsers:
                        self.operand_parsers[parser.hash] = parser.parser
                        self.additional_lexer_symbols.update(
                            parser.lexer_symbols)

        if tuple(self.section_names) == ():
            self.current_section = ast.Section('')
            self.sections = {'': self.current_section}
        else:
            self.sections = {
                name: ast.Section(name)
                for name in self.section_names
            }
            self.current_section = None

        # Map: symbol name -> ast.Symbol instance
        self.symbols = {}

    def get_undefined_symbol(self, name):
        return self.define_symbol(name)

    def define_symbol(self, name, location=None):
        try:
            symbol = self.symbols[name]
        except KeyError:
            symbol = self.symbols[name] = ast.Symbol(name, location)
        else:
            if location and symbol.location:
                raise AssemblyError(
                    'Already defined symbol: {} '
                    '(previously defined here: {})'.format(
                        name, repr_location(symbol.location)
                    ), location
                )
            symbol.location = location
        return symbol

    def _build_parser(self, input):
        mnemonics = set(self.opcodes)
        data_types = set(self.data_types)
        reserved = {'section'}

        bad_data_types = data_types & reserved
        if bad_data_types:
            raise BadAssemblerError(
                'Reserved keywords cannot be data types: {}'.format(
                    ' '.join(bad_data_types)
                )
            )

        class Lexer(BaseLexer):
            symbols = {'.', '@', ',', ':'} | self.additional_lexer_symbols
            line_comment_delimiter = ';'
            keywords = data_types | reserved
            yield_newlines = True

            if not self.case_sensitive:
                # If the assembler is case insensitive, lower all keywords and
                # tokens that follow a dot.

                def __init__(self, *args, **kwargs):
                    super(Lexer, self).__init__(*args, **kwargs)
                    self.lower_next_token = False

                def __next__(self):
                    result = super(Lexer, self).__next__()
                    if result == Symbol('.'):
                        self.lower_next_token = True
                    if (result == keyword or
                        result == identifier and self.lower_next_token
                    ):
                        result.value = result.value.lower()
                        self.lower_next_token = False
                    return result

        lexer = Lexer(input)

        # Empty line are ignored
        parser = fpl.skip(fpl.a(newline))

        # Symbols definition
        parser |= (
            fpl.memoize(fpl.a(identifier)) + fpl.a(Symbol(':')) +
            fpl.skip(fpl.a(newline))
        ) >> self._handle_symbol

        # Data types usage
        for data_type in self.data_types.values():
            data_parser = (
                fpl.memoize(fpl.a(Symbol('.'))) +
                fpl.a(Identifier(data_type.name))
            )
            for operand in data_type.operands:
                data_parser += fpl.a(operand)
            data_parser += fpl.skip(fpl.a(newline))
            parser |= data_parser >> self._handle_data(data_type)

        # General form for instructions
        operand_parser = or_all(
            # Sort the operand by hash since:
            # 1 - operand that has a low hash are less complex that high ones
            # 2 - complex operand parsers must be tried first
            # TODO: fix funcparserlib to improve its grammar-branch finding
            # more cleverly...
            self.operand_parsers[i]
            for i in reversed(sorted(self.operand_parsers))
        )
        instr_parser = fpl.memoize(fpl.a(identifier))
        if operand_parser:
            instr_parser += fpl.maybe(
                operand_parser +
                fpl.many(fpl.skip(fpl.a(Symbol(','))) + operand_parser)
            )
        instr_parser += fpl.skip(fpl.a(newline))
        parser |= instr_parser >> self._handle_instr

        # Section switches if sections are enabled.
        if self.section_names != ():
            parser |= (
                fpl.memoize(fpl.a(Symbol('.'))) + fpl.a(Keyword('section')) +
                fpl.a(identifier) + fpl.skip(fpl.a(newline))
            ) >> self._handle_section

        return (lexer, fpl.many(parser) + fpl.eof)

    def _handle_section(self, tokens):
        loc = get_sequence_location(tokens)
        section_name = tokens[-1].value
        if not self.case_sensitive:
            section_name = section_name.lower()
        try:
            self.current_section = self.sections[section_name]
        except KeyError:
            if  self.section_names:
                raise AssemblyError(
                    'Invalid section name: {}'.format(section_name), loc)
            else:
                self.current_section = self.sections[section_name] = \
                    ast.Section(section_name)
        return self.current_section

    def _handle_data(self, data_type):
        def _handle(tokens):
            loc = get_sequence_location(tokens)
            if not self.current_section:
                raise AssemblyError(
                    'Cannot define anything out of any section', loc)
            values = [token.value for token in tokens[2:]]
            try:
                data_type.validate(*values)
            except ValueError as e:
                raise AssemblyError(str(e), loc)
            self.current_section.add(ast.Data(data_type, loc, *values))

        return _handle

    def _handle_instr(self, tokens):
        mnemonic = tokens[0]
        if not self.case_sensitive:
            mnemonic.value = mnemonic.value.lower()
        operands = []
        # Operand list must be made linear. This is deep because of the grammar
        # topology.
        if len(tokens) > 1 and tokens[1]:
            tokens = tokens[1]
            operands.append(tokens[0])
            if len(tokens) > 1:
                operands.extend(tokens[1])

        loc = Location(
            mnemonic.location.start,
            operands[-1][0].end if operands else mnemonic.location.end
        )
        if not self.current_section:
            raise AssemblyError(
                'Cannot define anything out of any section', loc)

        try:
            matching_mnemonics = self.opcodes[mnemonic.value]
        except KeyError:
            raise AssemblyError(
                'No such mnemonic: {}'.format(mnemonic.value),
                mnemonic.location
            )
        matching_operands = [
            opcode for opcode in matching_mnemonics
            if opcode.is_matching(operands)
        ]

        if not matching_operands:
            raise AssemblyError(
                '{} has no such arguments'.format(mnemonic.value),
                mnemonic.location
            )
        if len(matching_operands) > 1:
            raise BadAssemblerError(
                'Some opcodes are ambiguous:\n{}\n'
                'The instruction at {} cannot be identified.'
                .format(
                    '\n'.join(
                        '- {}'.format(opcode) for opcode in matching_operands
                    ),
                    repr_location(mnemonic.location)
                )
            )

        self.current_section.add(ast.Opcode(
            matching_operands[0], loc, parsed_operands=operands
        ))

    def _handle_symbol(self, tokens):
        loc = get_sequence_location(tokens)
        if not self.current_section:
            raise AssemblyError(
                'Cannot define anything out of any section', loc)

        symbol = self.define_symbol(tokens[0].value, loc)
        self.symbols[symbol.name] = symbol
        self.current_section.add(symbol)

    def run(self, input, output):
        fpl.Parser.__str__ = fpl.Parser.__unicode__
        lexer, parser = self._build_parser(input)

        # TODO: fix funcparserlib to make it accept "location" as a token
        # position.
        class Token(BaseToken):
            __slots__ = ('type', 'value', 'location', 'pos')
            def __init__(self, *args, **kwargs):
                super(Token, self).__init__(*args, **kwargs)
                self.pos = self.location

        def map_token(tok):
            return Token(tok.type, tok.value, tok.location)
        lexer = map(
            map_token, #lambda tok: Token(tok.type, tok.value, tok.location),
            lexer
        )

        # TODO: fix funcparserlib to make it accept an iterator instead of a
        # list, and to correctly serialize its parsers.

        # This will parse the input file and build a kind of AST in
        # self.sections containing:
        # - data
        # - symbols
        # - opcodes (with unconverted operands)
        parser.parse(list(lexer))

        # Set an address to everything...
        self.locate_sections()
        # ... check that every section has an address
        for section in self.sections.values():
            if section.address is None:
                raise BadAssemblerError(
                    'Not every section has been set an address')

        # ... and check that every symbol has been defined if undefined symbols
        # are not allowed.
        if not self.allow_undefined_symbols:
            for symbol in self.symbols.values():
                if symbol.address is None:
                    raise AssemblyError(
                        'Undefined symbol: {}'.format(symbol.name))

        # Convert opcodes’ operands
        self.convert_operands()

        # And output the resulting binary.
        self.write_binary(output)

    def locate_sections(self):
        '''
        Set the starting address of each section.

        This enables the assembler to perform symbols resolution.
        '''
        raise NotImplementedError()

    def convert_operands(self):
        '''
        Convert operands from freshly parsed form to suitable one for opcodes
        encoding.
        '''
        for section in self.sections.values():
            for item in section.content:
                if not isinstance(item, ast.Opcode):
                    continue

                operands = []
                for parsed_operand, operand_type in zip(
                    item.parsed_operands, item.type.operands
                ):
                    operands.append(operand_type.convert(parsed_operand, self))
                item.operands = operands

    def write_binary(self):
        '''
        Write the final binary to a file.
        '''
        raise NotImplementedError()
