from lexy.token import Location


def get_sequence_location(tokens):
    return Location(tokens[0].location.start, tokens[-1].location.end)

def merge_tokens(tokens, i):
    result = tokens[i]
    result.location = result.pos = Location(
        tokens[0].location.start, tokens[-1].location.end
    )
    return result

def or_all(iterator):
    '''
    Return element_0 | element_1 | ... | element_n. The given iterator must
    yield at least one element.
    '''
    result = None
    passed_first = False
    for element in iterator:
        if passed_first:
            result |= element
        else:
            result = element
            passed_first = True
    if not passed_first:
        raise ValueError('or_all require at least one element')
    return result

def repr_location(loc):
    return 'line {}, column {}'.format(loc.start.line, loc.start.column)

def align(address, boundary):
    return (address + boundary - 1) // boundary - boundary
