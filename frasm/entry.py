import argparse
import sys

from funcparserlib.parser import ParserError
from lexy import LexingError

from frasm.assembler import BadAssemblerError, AssemblyError


def exit(status_code, message, *args, **kwargs):
    print(message.format(*args, **kwargs), file=sys.stderr)
    sys.exit(status_code)

def format_location(location):
    return 'Line {}, column {}'.format(*location.start)
def format_error(prefix, exc):
    # 'exc' must have:
    #  - either 1 argument (the error message)
    #  - or 2 arguments (the error message and the error location)
    result = exc.args[0]
    if len(exc.args) >= 2:
        result = '{}: {}'.format(format_location(exc.args[1]), result)
    return prefix + ': ' + result


arg_parser = argparse.ArgumentParser(
    description='Assemble a source file'
)
arg_parser.add_argument(
    'source', metavar='file', type=argparse.FileType('r'),
    help='the source to assemble')
arg_parser.add_argument(
    '--output', '-o', dest='output',
    type=argparse.FileType('wb'), default='bin',
    help='the output file (default: bin)')


def main(assembler, args):
    args = arg_parser.parse_args(args)

    try:
        assembler.run(args.source, args.output)
    except BadAssemblerError as e:
        exit(3, format_error('Incorrect assembler', e))
    except (LexingError, ParserError, AssemblyError) as e:
        exit(1, format_error('Error', e))
